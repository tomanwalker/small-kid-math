
var random = function(min, max){
	var result = Math.floor(Math.random() * (max - min + 1)) + min;
	console.log('random - %s / %s => %s', min, max, result);
	return result;
};

var shuffle = function(list){
	return list.map(function(x){
		return {
			key: Math.random(),
			val: x
		};
	}).sort((a, b) => a.key - b.key)
	.map(x => x.val);
};

var taskGenerator = {};

taskGenerator['+'] = function(amount, range){
	var nums = [];
	
	// make sure Total sum is less than Range_Max
	var first_max = (range[1] - (amount-1)); // e.g. 5 - (2-1) = 5-1 = 4
	var num_1 = random(range[0], first_max);
	var left = range[1] - num_1;
	nums.push( num_1 );
	console.log('generateTask - num_1 = %s | left = %s | first_max = %s', 
		num_1, left, first_max);
	
	for(var i=1; i<amount; i++){
		var next_max = left - (amount-1-i);
		var num_next = random(range[0], next_max);
		nums.push( num_next );
		left = left - Math.max(num_next, range[0]);
		console.log('generateTask - i = %s | num_x = %s | left = %s | next_max = %s', 
			i, num_next, left, next_max);
	}
	
	return nums;
};

taskGenerator['-'] = function(amount, range){
	
	var nums = [];
	
	var num_min = range[0] + (amount-1);
	var num_1 = random(num_min, range[1]);
	nums.push( num_1 );
	
	for(var i=1; i<amount; i++){
		var next_max = nums[i-1] - (amount-1); // 5 -1
		var num_next = random(range[0], Math.max(next_max, range[0]));
		nums.push( num_next );
	}
	
	return nums;
};

var generateTask = function(amount, range, sign){
	
	console.log('generateTask - start - %s | %s | %s', amount, range.join(','), sign);
	var nums = taskGenerator[sign](amount, range);
	
	if( ['+', '*'].includes(sign) ){
		nums = shuffle(nums);
	}
	console.log('generateTask - nums = %j', nums);
	
	var obj = {
		task: nums.join(` ${sign} `),
		check: 0
	};
	
	obj.check = nums.reduce(function(a,b){
		
		var res = a + b;
		
		if( sign === '-' ){
			res = a - b;
		}
		
		return res;
	});
	
	return obj;
};

var keepArrayLimit = function(list, limit){
	var len = list.length;
	
	if( len > limit ){
		var pointerStart = len - limit - 1;
		return list.slice( pointerStart );
	}
	
	return list;
};


