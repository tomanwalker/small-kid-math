
// ## config
var log = [];

// ## fund
var renderTask = function(){
	var result = generateTask(AMOUNT, RANGE, SIGN);
	
	// prevent duplicates
	var log_len = log.length;
	if( log_len > 0 ){
		var last = log[log_len-1];
		if( last.task === result.task ){
			result = generateTask(AMOUNT, RANGE, SIGN);
		}
	}
	
	$("#task").text( result.task );
	$("#check").val( result.check );
};

var checkTask = function(){
	var check = Number( $("#check").val() );
	var answer = Number( $("#answer").val() );
	var task = $("#task").text();
	var obj = {
		correct: false,
		ts: new Date(),
		task: task,
		answer: answer,
		check: check
	};
	
	console.log('checkTest - answer = %s | check = %s', answer, check);
	var helper_text = task + ' = ' + check;
	
	if( check === answer ){
		$("#result").text("Good >> " + helper_text);
		obj.correct = true;
	}
	else {
		$("#result").text("Wrong >> " + helper_text);
	}
	
	$("#answer").val("");
	return obj;
};

$(document).ready(function(){
	
	$("#answer").focus();
	renderTask();
	
	$( "#form" ).submit(function( event ) {
		
		var obj = checkTask();
		log.push(obj);
		log = keepArrayLimit(log, 100);
		var corr = log.filter(x => x.correct).length;
		var p_cor = Math.round(corr / log.length * 100);
		$("#stats").text(`${corr} / ${log.length} (${p_cor} %)`);
		
		$('#task')
			.animate({'opacity': 0}, 1000, renderTask)
			.animate({'opacity': 1}, 1000);
		//setTimeout(renderTask, 1000);
		
		event.preventDefault();
	});
});


