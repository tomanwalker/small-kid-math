# Math 4 small kid

Web site with simple math questions. Multiple levels for different ages.

## Run
```
npm install
npm start
```

## Backlog
```
=== TODO ===

- Color
- Statistics page (local storage)

- Add/Sub up to 20
- Add/Sub up to 100

- Multiply up to 10
- Multiply/Divide up to 20
- Mix up to 100

- Statistics page (username / session / database)

=== DONE ===

[08/2022]
- Sub (10)

[07/2022]
- Template Task page?
- Statistics (memory)
- Addition up to 5

```



