
// ## dependencies
var fs = require('fs');

// ## config

// ## exp
var ns = {};
module.exports = ns;

// ## func
ns.render = function(page, bag, template){ 
	
	var VIEW_EXT = 'html';
	if( typeof(template) === 'undefined' ){
		var template = 'template';
	}
	
	var pageContent = null;
	try{
		pageContent = fs.readFileSync('views/' + page + '.' + VIEW_EXT, 'utf8');
	}
	catch(err){
		throw err;
	}
	
	var mainContent = fs.readFileSync('views/' + template + '.' + VIEW_EXT, 'utf8');
	var merged = mainContent.replace('{{body}}', pageContent);
	
	for(var p in bag){
		var a = bag[p];
		if( typeof(a) === 'object' ){
			a = JSON.stringify(a);
		}
		//console.log('rander - replace - p = %s | bag = %s', p, a);
		merged = merged.replace('{{' + p + '}}', a);
	}
	
	return merged;
};


