
// ## dependencies
var express = require('express');
var fs = require('fs');

var view = require('./lib/view');

// ## config
var config = {
	PORT: Number(process.env.PORT || 9000)
};
var PAGE_MAP = {
	'': 'index'
};

var server = express();
server.use(express.json());

// ## functions
var numberTask = function(req, res){
	if( req.query.amount ){
		res.locals.amount = Number(req.query.amount);
	}
	if( req.query.range ){
		res.locals.range = JSON.parse(req.query.range);
	}
	if( req.query.sign ){
		res.locals.sign = req.query.sign;
	}
	res.locals.upto = res.locals.range[1];
	
	var merged = view.render('number-task', res.locals);
	return res.send(merged);
};

// ## routes
server.get('/', function(req, res){
	return res.redirect('/add?amount=2&range=[1,5]&sign=%2b');
});

server.get('/add', function(req, res, next){
	
	console.log('/add - query = %j', req.query);
	res.locals.amount = 2;
	res.locals.range = [1,5];
	res.locals.sign = "+";
	res.locals.operand = 'Addition';
	
	return next();
}, numberTask);
server.get('/sub', function(req, res, next){
	
	console.log('/sub - query = %j', req.query);
	res.locals.amount = 2;
	res.locals.range = [1,5];
	res.locals.sign = "-";
	res.locals.operand = 'Substraction';
	
	return next();
}, numberTask);

server.get('*', function(req, res, next){
	
	var nonRelevant = (
		req.originalUrl.includes('.js')
		|| req.originalUrl.includes('.css')
		|| req.originalUrl.includes('.ico')
		|| req.originalUrl.includes('.woff')
	);
	if( nonRelevant ){
		return next();
	}
	
	console.log('router.base - start - req.originalUrl = %s', req.originalUrl);
	
	var page = req.originalUrl.substring(1);
	page = page.replace( '.html', '');
	if( PAGE_MAP[page] ){
		page = PAGE_MAP[page];
	}
	console.log('router.base - mapper - page = <%s>', page);
	
	try{ 
		var merged = view.render(page, res.locals);
		return res.send(merged);
	}
	catch(err){
		console.log('router.base - page.catch - err = %j', err);
		return next();
	}
	
});

var filesPath = __dirname + '/html';
console.log('filesPath = %s', filesPath);
server.use('/', express.static(filesPath));

// ## flow
console.log('main - server start - port = %s', config.PORT);
server.listen(config.PORT, function(){
	console.log('main - server started...');
});


